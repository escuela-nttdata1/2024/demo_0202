import { Component } from '@angular/core';
import { IPersona2 } from 'src/app/models/persona.interface';

@Component({
  selector: 'app-menu1',
  templateUrl: './menu1.component.html',
  styleUrls: ['./menu1.component.scss']
})
export class Menu1Component {
  listaPersonas: Array<IPersona2> = [{nombres:'Carlos', edad: 19}];

  recibir(data: any){
    console.log(data);
  }
}
