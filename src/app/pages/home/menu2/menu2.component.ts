import { Component } from '@angular/core';
import { IPersona2 } from 'src/app/models/persona.interface';

@Component({
  selector: 'app-menu2',
  templateUrl: './menu2.component.html',
  styleUrls: ['./menu2.component.scss']
})
export class Menu2Component {

  titulo: string = 'tabla componente 2';
  listaPersonas: Array<IPersona2> = [{nombres:'Carlos', edad: 19},{nombres:'Luz', edad: 14},{nombres:'Roberto', edad: 12},{nombres:'Maria', edad: 10},{nombres:'Jose', edad: 25}];


}
