import { Component, Input } from '@angular/core';
import { PersonaService } from 'src/app/_services/persona.service';
import { Persona } from 'src/app/models/persona.class';
import { IPersona, IPersona2, IPersona3 } from 'src/app/models/persona.interface';

@Component({
  selector: 'app-segundo',
  templateUrl: './segundo.component.html',
  styleUrls: ['./segundo.component.scss']
})
export class SegundoComponent {

  @Input() titulo: string = 'Mi primera ';
  @Input() dataTable: Array<IPersona2> =[];

  esMenor: boolean = true;

  persona: Persona = new Persona();
  persona2: IPersona2 = {nombres:'', edad: 19}
  persona3: IPersona3 = {edad:undefined, nombres:undefined};

  hobbies: any[] = ["bailar","cantar",0];
  hobbies2: Array<any> = ["bailar","cantar",false];


  constructor(public personaService:PersonaService){
  console.log(this.personaService.sumarNumero(5,5));
  }

}
