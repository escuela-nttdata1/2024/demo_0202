import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  formParat: FormGroup = new FormGroup({});


  constructor( private formBuilder: FormBuilder, private router: Router){
    this.formParat = this.formBuilder.group({
      usuario: '',
      password:''
    });
  }

  ngOnInit() {
    if(localStorage.getItem('user')){
      this.router.navigate(['/home']);
    }
  }


enviar(){
  console.log(this.formParat.getRawValue());

  if(this.formParat.get('usuario')?.value === 'admin'){
    this.router.navigate(['/home']);
  //  sessionStorage.setItem('user', this.formParat.get('usuario')?.value);

  localStorage.setItem('user', this.formParat.get('usuario')?.value);
  }
}

}
