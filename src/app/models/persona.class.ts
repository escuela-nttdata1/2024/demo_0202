import { IAnimal, ICarro } from "./animal.interface";

export class Persona {
    descripcion: string;
   private edad: number; // private Integer edad;  public Integer getEdad(){return this.edad;}
    carro: ICarro;


    constructor(){
        this.descripcion = '';
        this.edad = 0;
        this.carro = {color:'', modelo:''};
    }

    public getEdad():number{
        return this.edad;
    }


    public setEdad(edad:number){
         this.edad = edad;
    }

}