export interface IAnimal{
    descripcion: string;
    edad: number;
    carro: ICarro;
}

 export interface ICarro{
    modelo: string;
    color: string;
}