import { IAnimal, ICarro } from "./animal.interface";

export interface IPersona{

    nombres: string; // private String nombres;
    edad: number; // private Integer edad;
    estatura: number; // private Double estatura;
    fechaNacimiento: Date // private Date fechaNacimiento;
    hobbies: Array<string>; 
    hobbies2: string[],// private List<String> hobbies;
    esCaso: boolean; // private Boolean escasado;
    carros: Array<ICarro>;
    carro: ICarro; // private List<String>
    animal: IAnimal;



}

export interface IPersona2{
    nombres: string; // private String nombres;
    edad: number;
    nombreCompleto?: string;
}

export interface IPersona3{
    nombres: any; // private String nombres;
    edad: any;
}

